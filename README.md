# UnivariateSplines

[![pipeline status](https://gitlab.com/feather-ecosystem/UnivariateSplines/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/UnivariateSplines/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/UnivariateSplines/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/UnivariateSplines/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/UnivariateSplines/branch/master/graph/badge.svg?token=yxL5FDzXwr)](https://codecov.io/gl/feather-ecosystem:core/UnivariateSplines)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/UnivariateSplines)
