# UnivariateSplines
[![pipeline status](https://gitlab.com/feather-ecosystem/UnivariateSplines/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/UnivariateSplines/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/UnivariateSplines/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/UnivariateSplines/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/UnivariateSplines/branch/master/graph/badge.svg?token=yxL5FDzXwr)](https://codecov.io/gl/feather-ecosystem:core/UnivariateSplines)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/UnivariateSplines)

***

This package implements the functionality necessary for handling univariate splines in the [`Feather ecosystem`](https://gitlab.com/feather-ecosystem). The implementation includes:
* evaluation of splines and respective derivatives,
* h-, p-, and k-refinement,
* (quasi-)interpolation,
* plotting and
* Gauss-Legendre, Gauss-Lobatto and weighted quadrature rules
* and more features documented in the API.

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in further extensions of the functionality implemented in this package (like tensor product B-splines), please visit the main documentation of the [`ecosystem`](https://gitlab.com/feather-ecosystem).

For new users, we recommend to read the tutorial first and then review the API to learn about further implementational details like specialized iterators, low-level operators and data structures. Users interested in mathematical details of the implementation are kindly referred to the literature.

**Splines technology**
1. De Boor, Carl, et al. *A practical guide to splines*. Vol. 27. New York: springer-verlag, 1978.
2. Piegl, Les, and Wayne Tiller. *The NURBS book*. Springer Science & Business Media, 2012.
3. Schumaker, Larry. *Spline functions: basic theory*. Cambridge University Press, 2007.
4. Rogers, David F. *An introduction to NURBS: with historical perspective*. Elsevier, 2000.

**Weighted quadrature**
5. Sangalli, G., and Tani, M. *Matrix-free weighted quadrature for a computationally efficient isogeometric k-method*. Computer Methods in Applied Mechanics and Engineering 338 (Aug. 2018), 117–133.
6. Calabro, F., Sangalli, G., and Tani, M. *Fast formation of isogeometric Galerkin matrices by weighted quadrature*. Computer Methods in Applied Mechanics and Engineering 316 (Apr. 2017), 606–622.
7. Hiemstra, R. R., Sangalli, G., Tani, M., Calabro, F., and Hughes, T. J. *Fast formation and assembly of finite element matrices with application to isogeometric linear elasticity*. Computer Methods in Applied Mechanics and Engineering 355 (Oct. 2019), 234–260.


## Theoretical background
In this section we would like to give a short exposition of splines. Splines are piecewise polynomial functions with a prescribed smoothness between
the pieces. Consider an interval ``[a,b]`` partitioned into ``n_{el}`` elements by an increasing sequence of *break-points*
```math
  a = x_0 < ... < x_k < x_{k+1} < ... < x_{n_{el}} = b.
```
A spline ``s \; : \; [a,b] \mapsto \mathbb{R}`` is a piecewise polynomial function such that
1. ``s|_{[x_{k-1},x_{k}]} \in \mathrm{span}(1,x,...,x^p), \quad \text{for } k = 1,...,n_{el}.``
2. ``D^{(j)} s(x^-_k) = D^{(j)} s(x^+_k) \quad \text{for } j=0,1,...,r_k, \; k = 0,...,n_{el}.``
Here ``p`` is the polynomial degree of the individual pieces and ``r_k \in (0,...,p-1)`` is an integer that prescribes the smoothness at break-point ``x_k``. The values ``D^{(j)} s(x^-_0), \; j=0,...r_0,`` and ``D^{(j)} s(x^+_{n_{el}}), \; j=0,...r_{n_{el}},`` should be considered as (Hermite-like) end-point conditions that control the behavior at ``x_0`` and ``x_{n_{el}}``, respectively.

A natural and convenient basis in which to describe polynomial splines is given by *B-splines*. We denote with ``\mu_k = p-r_k`` the *knot-multiplicity* at break-point ``x_k`` and define the so-called *knot-vector* as
```math
  U = \left( U_i \right)_{i=1}^{n+p+1} =
  ( \underbrace{x_0, \ldots, x_0}_{\mu_0}, \ldots,
    \underbrace{x_k, ..., x_k}_{\mu_k},
    \underbrace{x_{k+1}, ..., x_{k+1}}_{\mu_{k+1}}, \ldots,  
    \underbrace{x_{n_{el}}, ..., x_{n_{el}}}_{\mu_{n_{el}}}
  )
```
B-spline basis functions, ``(B_{i,p}(x), \; i \in 1,...,n)``, are uniquely defined
by the knot-vector ``U`` and can be stably and efficiently evaluated through the
Cox-DeBoor recursion [1]:
* The algorithm starts with piecewise constants
  * ``B_{i,0}(x) = 1 \quad \text{if } x \in [U_{i},U_{i+1})``
  * ``B_{i,0}(x) = 0 \quad \text{otherwise}``
* and successively increases the polynomial degree, as well as the support, based on the formula
  * ``B_{i,p}(x) = (1 - \gamma_i(x)) B_{i,p-1}(x) +
               \gamma_{i+1}(x) B_{i+1,p-1}(x), \quad \text{with } \gamma_i(x) = \frac{U_{i+p} - x}{U_{i+p} - U_{i}}``.

The collection of B-splines span the space of polynomial splines denoted by
```math
  \mathbb{S}^p_U(a,b) := \mathrm{span}\left\{ B_{i,p}, \; i=1,...,n \right\}
```

B-splines have many important properties that are useful in design as well as in analysis. For example, they
1. are non-negative: ``B_{i,p}(x) \geq 0 \quad \text{for all } x \in (a,b)``
2. have Local support: ``B_{i,p}(x) = 0 \quad \text{for all } x \notin [U_i, U_{i+1}]``
3. have a partition of unity: ``\sum_{i=1}^n B_{i,p}(x) = 1 ``
4. are linearly independent
5. are at least ``C^{r_k}`` smooth at ``x=x_k``.
6. have a differentiation rule: ``B_{i,p}^\prime (x) = p \frac{B_{i,p-1}(x)}{U_{i+p}-U_i} - p\frac{B_{i+1,p-1}(x)}{U_{i+p+1}-U_{i+1}}``

The last property, in which derivatives of B-spline basis functions are written as differences of B-splines of one degree lower, implies that
```math
  \frac{d}{dx} \sum_{i=1}^{n} \alpha_i B_{i,p}(x) = \sum_{i=1}^{n-1} p\frac{\alpha_{i+1}-\alpha_{i}}{U_{i+p} - U_i} B_{i,p-1}
```
