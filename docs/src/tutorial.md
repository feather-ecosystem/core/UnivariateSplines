```@meta
DocTestSetup = quote
    using UnivariateSplines
end
```


# Tutorial
In the following we will define a mapping `F(x)`, where `F` is represented by an univariate B-spline on `[0...L]`. This mapping will interpolate a function `f(x) = 1 + sin(2π/L*x)` at the Greville abscissa. We will showcase the following functionalities:

* construction of [`KnotVector`](@ref)
* definition of [`Bspline`](@ref)
* interpolation of functions
* convenience functions for plotting
* refinement of spline spaces
* computation of derivatives
* numerical quadrature

!!! note
    This manual serves the purpose of an introduction to the main functionalities of the `UnivariateSplines` package. Please have a look at the reference to gain a deeper overview of the implemented features.

## Definition of a spline space
The space `S` of univariate B-splines is uniquely defined by the polynomial degree `p` and a
non-decreasing sequence called the knot vector, denoted by `U`.
```jldoctest tutorial1
julia> L = 2.
2.0

julia> p = Degree(2)
2

julia> U = KnotVector([0.0,0.5,1.0,1.5,L], [p+1,1,1,1,p+1])
9-element SortedSequences.NonDecreasingVector{Float64}:
 0.0
 0.0
 0.0
 0.5
 1.0
 1.5
 2.0
 2.0
 2.0

julia> S = SplineSpace(p, U)
SplineSpace(degree = 2, interval = [0.0, 2.0], dimension = 6)
```

Here we defined the [`KnotVector`](@ref) by the breakpoints and their respective multiplicity. We can reconstruct `p` and `U` from a [`SplineSpace`](@ref) object by calling
```jldoctest tutorial1
julia> Degree(S)
2

julia> KnotVector(S)
9-element SortedSequences.NonDecreasingVector{Float64}:
 0.0
 0.0
 0.0
 0.5
 1.0
 1.5
 2.0
 2.0
 2.0
```

The dimension of a B-spline space `S` follows from `dim = length(U) - p - 1`
```jldoctest tutorial1
julia> dimsplinespace(S)
6
```

We provide a convenience function for plotting elements in the function space `S`
```@example tutorial1
using UnivariateSplines, Plots
L = 2.
p = Degree(2)
U = KnotVector([0.0,0.5,1.0,1.5,L], [p+1,1,1,1,p+1])
S = SplineSpace(p, U)
plot(S, legend=false)
```

## B-spline mappings
An important concept, which is heavily utilized inside the Feather ecosystem is the concept of a *mapping*, see [`FeaInterfaces`](https://gitlab.com/feather-ecosystem/core/FeaInterfaces) for more details. A mapping between two vector spaces is constructed on top of a function space (in this case [`Bspline`](@ref) space) and can by defined as
```jldoctest tutorial1
julia> F = Mapping(Bspline, S; codimension=1)
Mapping{Bspline,1,1,Tuple{Bspline{Float64,Array{Float64,1}}}}((Bspline{Float64}(SplineSpace(degree = 2, interval = [0.0, 2.0], dimension = 6)),))
```
The first argument is a singleton identifying the map type and the second argument provides the underlying space. The `dimension` and `codimension` of the mapping are
```jldoctest tutorial1
julia> dimension(F)
1

julia> codimension(F)
1
```
After construction `F` it is yet to be fully initialized. It is still missing the coefficients used in the expansion in terms of the space `S`. In the following we will set these in such a way that `F` interpolates `f` at the Greville abscissa.

!!! note
    The `dimension` and `codimension` do not have to be equal! A map of a curve onto a three-dimensional space would have dimension one and codimension three. Mappings of higher dimensions will be introduced in the documentation of the `TensorProductBsplines` package.

## Interpolation

The so-called *Greville* abscissa is a set of points which are in one-to-one correspondence with the B-spline basis functions. The abscissa follows from the formula `g[k] = 1/p*(U[k+1] + ... + U[k+p]),  k=1,...,n`. It is useful for example in interpolation, since the interpolation at these sites is stable and involves a square and invertible collocation matrix.
```jldoctest tutorial1
julia> g = grevillepoints(S)
6-element SortedSequences.IncreasingVector{Float64}:
 0.0
 0.25
 0.75
 1.25
 1.75
 2.0

julia> g = grevillepoints(p,U)
6-element SortedSequences.IncreasingVector{Float64}:
 0.0
 0.25
 0.75
 1.25
 1.75
 2.0
```

The matrix collocating the B-spline basis functions at the Greville abscissa can be obtained by
```jldoctest tutorial1
julia> B = Matrix(bspline_interpolation_matrix(p, U, grevillepoints(p,U), p+1)[1])
6×6 Array{Float64,2}:
 1.0   0.0    0.0    0.0    0.0    0.0
 0.25  0.625  0.125  0.0    0.0    0.0
 0.0   0.125  0.75   0.125  0.0    0.0
 0.0   0.0    0.125  0.75   0.125  0.0
 0.0   0.0    0.0    0.125  0.625  0.25
 0.0   0.0    0.0    0.0    0.0    1.0
```
By stating an interpolation problem `B⋅c = y` we can compute the coefficients of the mapping `F` which will give us the expansion in terms of the underlying B-spline space of degree `p`
```jldoctest tutorial1
julia> f(x) = 1+sin(2π/L*x)
f (generic function with 1 method)

julia> y = f.(g)
6-element Array{Float64,1}:
 1.0
 1.7071067811865475
 1.7071067811865475
 0.29289321881345254
 0.2928932188134523
 0.9999999999999998

julia> F[1].coeffs[:] = B\y
6-element Array{Float64,1}:
 1.0
 1.9428090415820636
 1.9428090415820631
 0.05719095841793676
 0.05719095841793648
 0.9999999999999998
```

A canonical `UnivariateSplines` way of performing a projection at the Greville abscissa would be
```jldoctest tutorial1
julia> project!(f, F[1], Interpolation())
6-element Array{Float64,1}:
 1.0
 1.9428090415820636
 1.9428090415820631
 0.05719095841793676
 0.05719095841793648
 0.9999999999999998
```
This in-place projection operator sets the coefficients of the mapping `F` in dimension `1` in a similar manner as the previous approach.

!!! note
    The canonical way is the preferred way. The preceding exposition was just for demonstration purposes.

By construction our completely defined mapping should reproduce the function `f(x)` at the Greville abscissa
```jldoctest tutorial1
julia> yref = F(g)
6-element Array{Float64,1}:
 1.0
 1.7071067811865475
 1.7071067811865475
 0.29289321881345254
 0.2928932188134523
 0.9999999999999998

julia> y ≈ yref
true
```

We provide evaluation routines with improved performance if the mapping is to be evaluated many times. The evaluation requires allocation of memory for results cache, which can be used time after time.
```jldoctest tutorial1
julia> yref = zeros(length(g))
6-element Array{Float64,1}:
 0.0
 0.0
 0.0
 0.0
 0.0
 0.0
```
The evaluation boils down to calling the `@evaluate!` macro
```jldoctest tutorial1
julia> @evaluate! yref = F(g)
6-element Array{Float64,1}:
 1.0
 1.7071067811865475
 1.7071067811865475
 0.29289321881345254
 0.2928932188134523
 0.9999999999999998

julia> y ≈ yref
true
```

We can also evaluate the mapping at arbitrary points
```jldoctest tutorial1
julia> using SortedSequences

julia> x = IncreasingVector(collect(0:0.1:L))
21-element IncreasingVector{Float64}:
 0.0
 0.1
 0.2
 0.3
 0.4
 0.5
 0.6
 0.7
 0.8
 0.9
 ⋮
 1.2
 1.3
 1.4
 1.5
 1.6
 1.7
 1.8
 1.9
 2.0

julia> F(x)
21-element Array{Float64,1}:
 1.0
 1.3394112549695432
 1.6033977866125206
 1.791959594928933
 1.9050966799187807
 1.9428090415820634
 1.9050966799187807
 1.791959594928933
 1.6033977866125202
 1.3394112549695425
 ⋮
 0.3966022133874796
 0.20804040507106678
 0.09490332008121927
 0.057190958417936616
 0.09490332008121917
 0.20804040507106658
 0.3966022133874794
 0.6605887450304567
 0.9999999999999998
```

Having all the ingredients ready let us have a look at the complete example and plot `F(x)` and `f(x)`
```@example tutorial1
using UnivariateSplines, SortedSequences, Plots

# define input data
L = 2.
f(x) = 1+sin(2π/L*x)

# define spline space
p = Degree(2)
U = KnotVector([0.0,0.5,1.0,1.5,L], [p+1,1,1,1,p+1])
S = SplineSpace(p, U)

# define Bspline map
F = Mapping(Bspline, S; codimension=1)
project!(f, F[1], Interpolation())

# evaluate
x = IncreasingVector(collect(0:0.01:L))
y = F(x)
yref = f.(x)

# plot
plot(x, [y, yref], label=["F(x)" "f(x)"])
scatter!(grevillepoints(S), F(grevillepoints(S)), label="F(g)")
```

## Refinement
A B-spline space can be h-, p-, or k-refined. All the refinement strategies are easily applied by calling the `refine` operator.

The spline space defined previously consists of 4 elements on which `p+1` functions are non-zero.
```jldoctest tutorial1
julia> num_elements(S.U)
4
```
We can reduce the error of the interpolation by refining the space
```jldoctest tutorial1
julia> S′ = refine(S, pRefinement(1))
SplineSpace(degree = 3, interval = [0.0, 2.0], dimension = 10)

julia> S′ = refine(S′, hRefinement(1))
SplineSpace(degree = 3, interval = [0.0, 2.0], dimension = 14)

julia> num_elements(S′.U)
8
```
The first refinement call elevates the degree and the second call halves the element sizes. Both increase the dimension of the space and thus improve the approximation power. The low-level function being called in the background is [`UnivariateSplines.refinement_operator`](@ref). Let's see how well `F(x)` interpolates `f(x)`
```@example tutorial1
# refine
S′ = refine(S, pRefinement(1))
S′ = refine(S′, hRefinement(1))
F = Mapping(Bspline, S′; codimension=1)
project!(f, F[1], Interpolation())

# evaluate
x = IncreasingVector(collect(0:0.01:L))
y = F(x)
yref = f.(x)

# plot
plot(x, [y, yref], label=["F(x)" "f(x)"])
scatter!(grevillepoints(S), F(grevillepoints(S)), label="F(g)")
```

As a matter of fact, the previous order of p- and h-refinement is analogous to the k-refinement strategy. We could obtain the same result by calling
```jldoctest tutorial1
julia> S = refine(S, kRefinement(1,1))
SplineSpace(degree = 3, interval = [0.0, 2.0], dimension = 14)

julia> Degree(S) == Degree(S′)
true

julia> KnotVector(S) == KnotVector(S′)
true
```

## Derivatives
Now that we have an acceptable approximation of `f(x)`, we can proceed with computing `dF(x)/dx` and comparing that derivative to `df(x)/dx`. In a similar manner to the evaluation of a mapping, the derivative can be evaluated using a lazy gradient operator
```jldoctest tutorial1
julia> ∇F = gradient(F)
1×1 FeaInterfaces.Lazy{FeaInterfaces.Gradient,Mapping{Bspline,1,1,Tuple{Bspline{Float64,Array{Float64,1}}}},2}:
 (Bspline{Float64}(SplineSpace(degree = 2, interval = [0.0, 2.0], dimension = 6)), (1,))
```
We can visualize the derivative and compare to the analytical one
```@example tutorial1
x = IncreasingVector(collect(0:0.01:L))

∇f(x) = 2π/L*cos(2π/L*x)
yref = ∇f.(x)

∇F = gradient(F)
y = [zeros(size(x)) for k in 1:dimension(F)]
@evaluate! y = ∇F(x)

plot(x,[y[1] yref], label=["dF(x)/dx" "df(x)/dx"])
plot!(x, F(x), linecolor="gray", linestyle=:dot, label="F(x)")
```
!!! note
    This example showcases a more general use of the operators. The gradient operator can be applied to mappings (and spaces) of higher (co)dimensions and will provide an array under evaluation.
    
## Numerical quadrature
We provide a set of numerical quadrature rules (Gauss-Legendre, Gauss-Lobatto and weighted quadrature), which can be generated on the interval of a spline space. In the following example we will generate a global Gauss-Legendre quadrature rule 
```jldoctest tutorial1
julia> Q = PatchRule(S; method=Legendre)
PatchRule{Legendre,Float64}([1, 5, 9, 13, 17, 21, 25, 29, 33], [0.01735796105074343, 0.08250236955189297, 0.16749763044810703, 0.23264203894925656, 0.2673579610507434, 0.33250236955189294, 0.41749763044810706, 0.48264203894925656, 0.5173579610507434, 0.5825023695518929  …  1.417497630448107, 1.4826420389492567, 1.5173579610507435, 1.582502369551893, 1.667497630448107, 1.7326420389492565, 1.7673579610507435, 1.832502369551893, 1.917497630448107, 1.9826420389492565], [0.04348185564218173, 0.08151814435781828, 0.08151814435781828, 0.04348185564218173, 0.04348185564218173, 0.08151814435781828, 0.08151814435781828, 0.04348185564218173, 0.04348185564218173, 0.08151814435781828  …  0.08151814435781828, 0.04348185564218173, 0.04348185564218173, 0.08151814435781828, 0.08151814435781828, 0.04348185564218173, 0.04348185564218173, 0.08151814435781828, 0.08151814435781828, 0.04348185564218173])

```
Having the evaluation sites `Q.x` and the corresponding weights `Q.w` we can perform numerical integration over the complete domain in a straightforward manner
```jldoctest tutorial1
julia> Q.w' * f.(Q.x)
2.0

julia> Q.w' * F(Q.x)
2.0
```
!!! note
    Please note that in contrast to standard codes where the quadrature rule is implemented on a reference element, the generated *global* quadrature rule is already mapped on the spline domain by an affine map. By that a function `f(x) on [0,L]` can be integrated on `[0,L]` by `∫f(x)dx = sum(Q.w[k] * f(Q.x[k])), k = 1,...,length(Q.w)`. There are no additional geometrical factors involved!
