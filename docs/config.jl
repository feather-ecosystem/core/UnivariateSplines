using UnivariateSplines, FeaInterfaces

package_info = Dict(
    "modules" => [UnivariateSplines, FeaInterfaces],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "UnivariateSplines.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/UnivariateSplines",
    "pages" => [
        "About"  =>  "index.md"
        "Tutorial" => "tutorial.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(UnivariateSplines, :DocTestSetup, :(using UnivariateSplines, FeaInterfaces, LinearAlgebra, SortedSequences, SparseArrays); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(UnivariateSplines, fix=true)
end

# disable display support for `Plots`
# (important for plotting on headless machines, i.e. gitlab runner)
ENV["GKSwstype"] = "nul"
